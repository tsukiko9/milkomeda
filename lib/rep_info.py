# -*- coding: utf-8 -*-
# @Time           : 2018/10/10 3:12 PM
# @Author         : Cirno
# @File           : rep_info.py
# @Description    ：


async def make_msg(code, info, data=None):
    rep_msg = dict()
    rep_msg['status_code'] = code
    rep_msg['info'] = info
    if data is not None:
        rep_msg['data'] = data

    return rep_msg
