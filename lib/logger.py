# -*- coding: utf-8 -*-
# @Time           : 2018/9/17 下午2:17
# @Author         : Cirno
# @File           : logger.py
# @Description    ：
import datetime


from module.conf import ServerConf


async def logger(msg, msg_type=None):
    is_debug = ServerConf().is_debug
    if msg_type is 'e':
        log_type = 'ERROR'
    elif msg_type is 's':
        log_type = 'SUCCESS'
    elif msg_type is 'w':
        log_type = 'WARN'
    elif msg_type is 'd' and is_debug:
        log_type = 'DEBUG'
    elif msg_type is None:
        log_type = 'INFO'
    else:
        return

    log_str = '[' + str(datetime.datetime.now()) + ']' + '[' + log_type + ']' + ' : ' + msg
    await logger_write('log.log', log_str)


async def logger_write(file_name, log_msg):
    with open(file_name, 'a+', encoding='utf-8') as f:
        f.write(log_msg + '\n')
