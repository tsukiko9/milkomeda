# -*- coding: utf-8 -*-
# @Time           : 2018/10/20 9:17
# @Author         : Cirno
# @File           : data_process.py
# @Description    ：
from lib.logger import logger
from module.conf import ServerConf


async def make_sqlite_data(data, is_multiple=False, is_desc=False):
    try:
        if isinstance(data, dict):
            # 正序合成list
            if not is_multiple:
                # 多参数处理, dict --> tuple --> list
                params = tuple(data.values())
                params_list = list()
                # 正序tuple
                if not is_desc:
                    params_list.append(params)
                # 倒序tuple
                else:
                    params_list.append(params[::-1])

                return params_list

            # 预留多数据处理
            # l2 = [t[::-1] for t in l]
            if is_multiple:
                pass
        else:
            raise Exception
    except Exception as e:
        if ServerConf().is_debug:
            await logger('生成sql参数list异常，接收数据为{0}，异常信息为{1}'.format(data, e), 'e')
        return
