# -*- coding: utf-8 -*-
# @Time           : 2018/10/23 2:26 PM
# @Author         : Cirno
# @File           : network.py
# @Description    ：
import json

from tornado.httpclient import AsyncHTTPClient

from lib.logger import logger
from module.conf import TunnelProxyConf, IPProxyConf


class AsyncClient:

    def __init__(self, retry=3):
        self._retry = retry

    # 隧道代理
    async def tunnel_exec(self, url):
        # 设置使用代理参数
        AsyncHTTPClient.configure(
            "tornado.curl_httpclient.CurlAsyncHTTPClient"
        )

        _proxy = TunnelProxyConf()
        client = AsyncHTTPClient()
        _proxy.headers['User-Agent'] = "Mozilla/5.0 (Windows NT 10.0; WOW64) " \
                                       "AppleWebKit/537.36 (KHTML, like Gecko)" \
                                       " Chrome/70.0.3538.67 Safari/537.36"
        for i in range(self._retry):
            try:
                rep = await client.fetch(url, method="GET", headers=_proxy.headers, proxy_host=_proxy.host,
                                         proxy_port=_proxy.port, connect_timeout=25.0, request_timeout=30.0,
                                         validate_cert=False)

                if rep.code == 200:
                    data_json = json.loads(rep.body.decode("gb2312"))
                    await logger('抓取数据成功，数据为：{0}'.format(data_json), 'd')
                    return data_json
                else:
                    await logger('代理ISP响应错误，正在重试第{0}次...'.format(i + 1), 'w')
                    continue

            except Exception as e:
                await logger('数据抓取异常，正在重试第{0}次，错误信息：{1}'.format(i + 1, e), 'e')
                continue

    # IP代理
    async def ip_exec(self, url):

        AsyncHTTPClient.configure(
            "tornado.curl_httpclient.CurlAsyncHTTPClient"
        )
        for i in range(self._retry):
            try:

                client = AsyncHTTPClient()

                _headers = dict()
                _headers['User-Agent'] = "Mozilla/5.0 (Windows NT 10.0; WOW64) " \
                                         "AppleWebKit/537.36 (KHTML, like Gecko)" \
                                         " Chrome/70.0.3538.67 Safari/537.36"
                # 获取代理API
                ip_pool = IPProxyConf().api
                # 获取代理IP
                ip_rep = await client.fetch(ip_pool, method="GET", headers=_headers, connect_timeout=25.0,
                                            request_timeout=30.0,
                                            validate_cert=False)

                if ip_rep.code == 200:
                    ip_data = json.loads(ip_rep.body.decode('utf-8'))

                    if not isinstance(ip_data, dict) and not ip_data['code'] == 0:
                        raise Exception
                    ip_list = ip_data['msg']
                    _ip_host = ip_list[0]['ip']
                    _ip_port = int(ip_list[0]['port'])
                    rep = await client.fetch(url, method="GET", headers=_headers, proxy_host=_ip_host,
                                             proxy_port=_ip_port, validate_cert=False, connect_timeout=25.0,
                                             request_timeout=30.0, )

                    if rep.code == 200:
                        data_json = json.loads(rep.body.decode("utf-8"))
                        await logger('抓取数据成功，数据为：{0}'.format(data_json), 'd')
                        return data_json
                    else:
                        await logger('代理ISP响应错误，正在重试第{0}次...'.format(i + 1), 'w')
                        continue
            except Exception as e:
                await logger('数据抓取异常，正在重试第{0}次，错误信息：{1}'.format(i + 1, e), 'e')
                continue
