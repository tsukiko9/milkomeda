# -*- coding: utf-8 -*-
# @Time           : 2018/10/10 10:45 AM
# @Author         : Cirno
# @File           : user.py
# @Description    ：
import json

from controller.global_ctrl import check_keys
from controller.user_ctrl import user_add, user_update_push_key, user_check_params_user_exist
from lib.logger import logger
from handlers.middleware import MiddleHandler
from lib.rep_info import make_msg


class UserHandlers(MiddleHandler):

    async def get(self):
        pass

    async def post(self, *args, **kwargs):

        try:
            # init data
            data = json.loads(self.request.body)

            # 校验是否存量用户
            is_exist = await user_check_params_user_exist(data['params'])

            # 新增用户，不校验基础key
            if data['method'] == 'add':
                if not is_exist:
                    event_msg = await user_add(data['params'])
                    return self.finish(event_msg)
                else:
                    return self.finish(await make_msg(210, '用户已存在'))

            # 校验用户基础key参数
            check_msg = await check_keys(data)
            if check_msg is not None:
                return self.finish(check_msg)

            # 更新第三方推送key
            if data['method'] == 'updateKey':
                event_msg = await user_update_push_key(data['params'])
                return self.finish(event_msg)

        except Exception as e:
            await logger('请求异常，数据为：{0}，错误信息：{1}'.format(self.request.body, e), 'e')
            return self.finish(await make_msg(400, '数据解析异常'))
