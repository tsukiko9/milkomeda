# -*- coding: utf-8 -*-
# @Time           : 2018/10/22 20:43
# @Author         : Cirno
# @File           : goods.py
# @Description    ：
import json

from controller.goods_ctrl import goods_check_url, goods_add
from handlers.middleware import MiddleHandler
from lib.logger import logger
from lib.rep_info import make_msg


class GoodsHandlers(MiddleHandler):
    async def get(self, *args, **kwargs):
        pass

    async def post(self, *args, **kwargs):

        try:
            # init data
            data = json.loads(self.request.body)

            if data['method'] == 'add':

                # 校验URL有效性
                check_msg = await goods_check_url(data['params']['goodsUrl'])
                if check_msg is not None:
                    return self.finish(check_msg)

                # 执行新增
                event_msg = await goods_add(data['params'])
                return self.finish(event_msg)
            else:
                return self.finish(await make_msg(400, '无效数据'))
        except Exception as e:
            await logger('请求异常，数据为：{0}，错误信息：{1}'.format(self.request.body, e), 'e')
            return self.finish(await make_msg(400, '数据解析异常'))
