# -*- coding: utf-8 -*-
# @Time           : 2018/10/15 22:07
# @Author         : Cirno
# @File           : middleware.py
# @Description    ：
import json

import tornado.web as web

from controller.global_ctrl import check_headers_token, check_is_dict, check_body, check_black_list, check_keys
from lib.logger import logger
from module.conf import ServerConf


class MiddleHandler(web.RequestHandler):

    async def prepare(self):

        try:
            data = json.loads(self.request.body)
            if isinstance(data, dict):

                # 校验token
                check_msg = await check_headers_token(self.request.headers)
                if check_msg is not None:
                    return self.finish(check_msg)

                # 校验黑名单
                check_msg = await check_black_list(data)
                if check_msg is not None:
                    return self.finish(check_msg)

                # 校验数据格式
                check_msg = await check_is_dict(data)
                if check_msg is not None:
                    return self.finish(check_msg)

                # 校验数据合法
                check_msg = await check_body(data)
                if check_msg is not None:
                    return self.finish(check_msg)

                # 新旧用户检查
                if not self.request.uri == '/user':
                    check_msg = await check_keys(data, check_push=True)
                    if check_msg is not None:
                        return self.finish(check_msg)

        except Exception as e:
            if ServerConf().is_debug:
                await logger('通用校验异常，异常信息为{0}'.format(e), 'e')
            return self.finish({'code': 500, 'info': '无效数据'})
