# -*- coding: utf-8 -*-
# @Time           : 2018/9/17 下午2:10
# @Author         : Cirno
# @File           : index.py
# @Description    ：
import tornado.web as web

from handlers.middleware import MiddleHandler
from lib.logger import logger


class IndexHandler(MiddleHandler):
    async def get(self, *args, **kwargs):
        pass
