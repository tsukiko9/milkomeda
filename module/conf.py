# -*- coding: utf-8 -*-
# @Time           : 2018/9/19 下午4:15
# @Author         : Cirno
# @File           : conf.py
# @Description    ：
import yaml


class ServerConf(object):
    def __init__(self):
        self._conf_file = yaml.load(open('./conf/server.yaml'))
        self._server_conf = self._conf_file['Server']
        self.port = self._server_conf['Port']
        self.db_path = self._server_conf['DBPath']
        self.is_debug = self._server_conf['DeBug']
        self.wechat_token = self._server_conf['WeChatToken']


class TunnelProxyConf(object):
    def __init__(self):
        self.__conf_file = yaml.load(open('./conf/server.yaml'))
        self.__proxy_conf = self.__conf_file['TunnelProxy']
        self.host = self.__proxy_conf['Host']
        self.port = self.__proxy_conf['Port']
        self.headers = self.__proxy_conf['Headers']


class IPProxyConf(object):
    def __init__(self):
        self.__conf_file = yaml.load(open('./conf/server.yaml'))
        self.__proxy_conf = self.__conf_file['IPProxy']
        self.api = self.__proxy_conf['API']
