# -*- coding: utf-8 -*-
# @Time           : 2018/9/20 上午11:21
# @Author         : Cirno
# @File           : global_dml.py
# @Description    ：
from db.sqlite import DBHelper
from lib.logger import logger
from module.conf import ServerConf


async def global_check_black_list(user_id):
    try:
        _dml = 'SELECT A.ID ID FROM USER A WHERE A.ID = ? AND A.RANK < 0'
        _bad_user = await DBHelper().execute(_dml, user_id)
        # 数据库中没有的用户
        if not _bad_user:
            return False
        # 已存在的用户
        elif _bad_user[0]['ID']:
            return True
        return False
    except Exception as e:
        if ServerConf().is_debug:
            await logger('[sql]执行【校验黑名单】错误，异常信息{0}'.format(e), 'e')
        return False


async def global_check_app_key(user_id):
    try:
        _dml = 'SELECT A.APP_KEY APP_KEY FROM USER A WHERE A.ID = ?'
        _app_key = await DBHelper().execute(_dml, user_id)
        # 数据库中没有的用户
        if not _app_key:
            return False
        # 已存在的用户检查APP KEY
        elif _app_key[0]['APP_KEY']:
            return True
        return False
    except Exception as e:
        if ServerConf().is_debug:
            await logger('[sql]执行【校验app key】错误，异常信息{0}'.format(e), 'e')
        return False


async def global_check_push_key(user_id):
    try:
        _dml = 'SELECT A.PUSH_KEY PUSH_KEY FROM USER A WHERE A.ID = ?'
        _push_key = await DBHelper().execute(_dml, user_id)
        # 数据库中没有的用户
        if not _push_key:
            return False
        # 已存在的用户检查PUSH KEY
        elif _push_key[0]['PUSH_KEY']:
            return True
        return False
    except Exception as e:
        if ServerConf().is_debug:
            await logger('[sql]执行【校验推送key】错误，异常信息{0}'.format(e), 'e')
        return False


async def global_check_user_exist(user_id):
    try:
        _dml = 'SELECT A.ID ID FROM USER A WHERE A.ID = ?'
        _user = await DBHelper().execute(_dml, user_id)
        # 数据库中没有的用户
        if not _user:
            return False
        # 已存在的用户检查APP KEY
        elif _user[0]['ID']:
            return True
        return False
    except Exception as e:
        if ServerConf().is_debug:
            await logger('[sql]执行【校验存量用户】错误，异常信息{0}'.format(e), 'e')
        return False
