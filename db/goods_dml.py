# -*- coding: utf-8 -*-
# @Time           : 2018/10/22 20:44
# @Author         : Cirno
# @File           : goods_dml.py
# @Description    ：
from db.sqlite import DBHelper
from lib.logger import logger
from module.conf import ServerConf


async def add_goods(url):
    try:
        _dml = 'INSERT INTO GOODS(URL) VALUES(?)'
        _app_key = await DBHelper().execute(_dml, url, is_commit=True)
        return True
    except Exception as e:
        if ServerConf().is_debug:
            await logger('[sql]执行【新增商品】错误，参数为{0}，异常信息{1}'.format(url, e), 'e')
        return False


async def add_user_goods(params):
    try:
        _dml = 'INSERT INTO USER_GOODS(USER_ID, GOODS_URL, JOIN_PRICE) VALUES(?, ?, ?)'
        _app_key = await DBHelper().execute(_dml, params_list=params, is_many=True, is_commit=True)
        return True
    except Exception as e:
        if ServerConf().is_debug:
            await logger('[sql]执行【商品关系】错误，参数为{0}，异常信息{1}'.format(params, e), 'e')
        return False


async def check_goods_exist(url):
    try:
        _dml = 'SELECT 1 FROM GOODS A WHERE A.URL = ?'
        _url = await DBHelper().execute(_dml, url)
        # 数据库中没有
        if not _url:
            return False
        # 已存在
        elif _url[0]['1']:
            return True
        return False
    except Exception as e:
        if ServerConf().is_debug:
            await logger('[sql]执行【校验商品URL有效性】错误，异常信息{0}'.format(e), 'e')
        return False


async def check_user_goods_exist(params):
    try:
        _dml = "SELECT 1 FROM USER_GOODS A WHERE A.USER_ID = '{0}' AND A.GOODS_URL = '{1}'".format(params[0][0],
                                                                                                   params[0][1])
        _url = await DBHelper().execute(_dml, is_dict=True)
        # 数据库中没有
        if not _url:
            return False
        # 已存在
        elif _url[0]['1']:
            return True
        return False
    except Exception as e:
        if ServerConf().is_debug:
            await logger('[sql]执行【校验用户商品URL有效性】错误，异常信息{0}'.format(e), 'e')
        return False
