# -*- coding: utf-8 -*-
# @Time           : 2018/9/17 下午3:31
# @Author         : Cirno
# @File           : sqlite.py
# @Description    ：
import sqlite3

from lib.logger import logger
from module.conf import ServerConf


def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d


class DBHelper:
    """
    thanks for
    http://www.cdotson.com/2014/06/generating-json-documents-from-sqlite-databases-in-python/
    """

    def __init__(self):
        self._conn = sqlite3.connect(ServerConf().db_path)

    async def execute(self, sql, *args, params_list=None, is_many=False, is_dict=True, is_commit=False):

        if is_dict:
            self._conn.row_factory = dict_factory
        else:
            self._conn.row_factory = None

        _cursor = self._conn.cursor()
        if not is_many:
            _cursor.execute(sql, args)
        else:
            # 多参数执行，list(tuple(a, b))
            if isinstance(params_list, list):
                _cursor.executemany(sql, params_list)
            else:
                await logger('调用executemany错误的参数类型', 'e')

        data = None

        if not is_commit:
            data = _cursor.fetchall()
        else:
            self._conn.commit()

        _cursor.close()
        return data
