# -*- coding: utf-8 -*-
# @Time           : 2018/10/14 16:48
# @Author         : Cirno
# @File           : user_dml.py
# @Description    ：
from db.sqlite import DBHelper
from lib.logger import logger
from module.conf import ServerConf


async def update_push_key(params):
    try:
        _dml = 'UPDATE USER SET PUSH_KEY = ? WHERE ID = ?'
        _push_key = await DBHelper().execute(_dml, params_list=params, is_many=True, is_commit=True)
        return True
    except Exception as e:
        if ServerConf().is_debug:
            await logger('[sql]执行【更新推送key】错误，参数为{0}，异常信息{1}'.format(params, e), 'e')
        return False


async def add_user(params):
    try:
        _dml = 'INSERT INTO USER(ID, APP_KEY) VALUES(?, ?)'
        _app_key = await DBHelper().execute(_dml, params_list=params, is_many=True, is_commit=True)
        return True
    except Exception as e:
        if ServerConf().is_debug:
            await logger('[sql]执行【新增用户】错误，参数为{0}，异常信息{1}'.format(params, e), 'e')
        return False


async def update_invitation_status(code):
    try:
        _dml = 'UPDATE INVITATION_CODE SET STATUS = 1 WHERE CODE = ?'
        _push_key = await DBHelper().execute(_dml, code, is_commit=True)
        return True
    except Exception as e:
        if ServerConf().is_debug:
            await logger('[sql]执行【更新邀请码状态】错误，参数为{0}，异常信息{1}'.format(code, e), 'e')
        return False


async def check_invitation_code(code):
    try:
        _dml = 'SELECT A.CODE CODE FROM INVITATION_CODE A WHERE A.CODE = ? AND A.STATUS = 0'
        _code = await DBHelper().execute(_dml, code)
        # 数据库中没有
        if not _code:
            return False
        # 已存在
        elif _code[0]['CODE']:
            return True
        return False
    except Exception as e:
        if ServerConf().is_debug:
            await logger('[sql]执行【校验app key有效性】错误，异常信息{0}'.format(e), 'e')
        return False
