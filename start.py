# -*- coding: utf-8 -*-
# @Time           : 2018/9/17 下午1:46
# @Author         : Cirno
# @File           : start.py
# @Description    ：

from app.app import application


if __name__ == '__main__':
    application()
