# -*- coding: utf-8 -*-
# @Time           : 2018/10/22 20:44
# @Author         : Cirno
# @File           : goods_ctrl.py
# @Description    ：
import random
import re

from db.goods_dml import check_goods_exist, add_goods, check_user_goods_exist, add_user_goods
from lib.data_process import make_sqlite_data
from lib.logger import logger
from lib.network import AsyncClient
from lib.rep_info import make_msg
from module.conf import ServerConf


async def goods_add(data):
    try:
        # 商品不存在
        if not await check_goods_exist(data['goodsUrl']):
            if not await add_goods(data['goodsUrl']):
                raise Exception

        params_list = await make_sqlite_data(data)

        # 关联关系不存在
        if not await check_user_goods_exist(params_list):
            # 重新整理数据
            params_dict = dict()
            params_dict['userId'] = data['userId']
            params_dict['goodsUrl'] = data['goodsUrl']
            join_price = await goods_get_join_price(data['goodsUrl'])
            if not isinstance(join_price, bool):
                params_dict['joinPrice'] = join_price
            else:
                raise Exception

            # 重新处理数据元组
            params_list = await make_sqlite_data(params_dict)

            # 写入关系表
            if await add_user_goods(params_list):
                return await make_msg(200, '商品添加成功')
        else:
            return await make_msg(202, '已关注该商品')

    except Exception as e:
        if ServerConf().is_debug:
            await logger('新增商品失败，接收数据为{0}，异常信息为{1}'.format(data, e), 'e')
        return await make_msg(201, '商品新增失败')


async def goods_get_join_price(url):
    try:
        reg_id = '[^(0-9)]'
        sku_id = re.sub(reg_id, '', url, 0, re.MULTILINE)

        goods_url = 'https://p.3.cn/prices/mgets?type=1&area=1_72_2819_0' \
                    '&pdtk=&pduid={0}&pdpin=&pin=null' \
                    '&pdbp=0&skuIds=J_{1}&ext=11100000&source=item-pc'

        pdu_id = random.randint(1, 100000000)

        # 获取当前价格
        # _data = await AsyncClient().tunnel_exec(goods_url.format(pdu_id, sku_id))
        _data = await AsyncClient().ip_exec(goods_url.format(pdu_id, sku_id))
        _data = dict(_data[0])
        if isinstance(_data, dict):
            return _data['p']

    except Exception as e:
        return False


async def goods_check_url(url):
    try:
        reg_url = '(https?|ftp|file)://[-A-Za-z0-9+&@#/%?=~_|!:,.;]+[-A-Za-z0-9+&@#/%=~_|]'
        if re.match(reg_url, url):
            return
        return await make_msg(400, '无效参数')
    except Exception as e:
        return False
