# -*- coding: utf-8 -*-
# @Time           : 2018/10/14 14:28
# @Author         : Cirno
# @File           : global_ctrl.py
# @Description    ：
from db.global_dml import global_check_app_key, global_check_push_key, global_check_black_list
from lib.logger import logger
from lib.rep_info import make_msg
from module.conf import ServerConf


async def check_headers_token(headers):
    token = headers.get('token')
    if not token == ServerConf().wechat_token:
        await logger('无效token，被解析的数据为：{0}'.format(headers.get('token')), 'e')
        return await make_msg(999, '我不认识你')


async def check_is_dict(data):
    if not isinstance(data, dict):
        await logger('不是有效的dict，数据为：{0}'.format(data), 'e')
        return await make_msg(500, '无效数据格式')
    return


async def check_body(data):
    try:
        if data['params']['userId'] is None:
            raise Exception
        return
    except Exception as e:
        await logger('body数据解析异常,数据为：{0},异常信息为：{1}'.format(data, e), 'e')
        return await make_msg(500, '无效数据')


async def check_keys(data, check_push=False):
    _check_app = await global_check_app_key(data['params']['userId'])
    if not _check_app:
        return await make_msg(501, '未授权用户，请先绑定邀请码')
    _check_push = await global_check_push_key(data['params']['userId'])
    if not _check_push and check_push:
        return await make_msg(502, '请先绑定第三方推送key')
    return


async def check_black_list(data):
    try:
        _bad_user = await global_check_black_list(data['params']['userId'])
        if not _bad_user:
            return
        return await make_msg(503, '已封禁')
    except Exception as e:
        await logger('黑名单数据解析异常,数据为：{0},异常信息为：{1}'.format(data, e), 'e')
        return await make_msg(500, '无效数据')
