# -*- coding: utf-8 -*-
# @Time           : 2018/10/14 14:06
# @Author         : Cirno
# @File           : user_ctrl.py
# @Description    ：

from db.global_dml import global_check_user_exist
from db.user_dml import add_user, update_push_key, check_invitation_code, update_invitation_status
from lib.data_process import make_sqlite_data
from lib.logger import logger
from lib.rep_info import make_msg
from module.conf import ServerConf


async def user_update_push_key(data):
    # 检查入参
    if not await user_check_params_push_key(data):
        return await make_msg(400, '无效参数')

    try:
        params_list = await make_sqlite_data(data, is_desc=True)
        if params_list is not None:
            if await update_push_key(params_list):
                return await make_msg(200, '推送key更新成功')
    except Exception as e:
        if ServerConf().is_debug:
            await logger('更新push key，接收数据为{0}，异常信息为{1}'.format(data, e), 'e')
        return await make_msg(201, '推送key更新失败')


async def user_add(data):
    # 检查入参
    if not await user_check_params_app_key(data):
        return await make_msg(400, '无效参数')
    if not await user_check_invitation_code(data['appKey']):
        return await make_msg(202, '邀请码无效')
    try:
        params_list = await make_sqlite_data(data)
        if params_list is not None and await user_set_invitation_status(data['appKey']):
            if await add_user(params_list):
                return await make_msg(200, '萌新加入成功')
        raise Exception
    except Exception as e:
        if ServerConf().is_debug:
            await logger('用户注册失败，接收数据为{0}，异常信息为{1}'.format(data, e), 'e')
        return await make_msg(201, '萌新注册失败')


async def user_set_invitation_status(code):
    try:
        if await update_invitation_status(code):
            return True
        raise Exception
    except Exception as e:
        return False


async def user_check_invitation_code(code):
    try:
        if await check_invitation_code(code):
            return True
        return False
    except Exception as e:
        return False


async def user_check_params_user_exist(data):
    try:
        if await global_check_user_exist(data['userId']):
            return True
        return False
    except Exception as e:
        return False


async def user_check_params_push_key(data):
    try:
        if not data['pushKey']:
            return False
        return True
    except Exception as e:
        return False


async def user_check_params_app_key(data):
    try:
        if not data['appKey']:
            return False
        return True
    except Exception as e:
        return False
