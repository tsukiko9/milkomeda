# -*- coding: utf-8 -*-
# @Time           : 2018/9/17 下午1:56
# @Author         : Cirno
# @File           : app.py
# @Description    ：

import tornado.web as web
import tornado.ioloop as ioloop

from app.router import routers
from module.conf import ServerConf


def application():
    conf = ServerConf()
    app = web.Application(routers)
    app.listen(conf.port)
    ioloop.IOLoop.current().start()
