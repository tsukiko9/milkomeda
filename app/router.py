# -*- coding: utf-8 -*-
# @Time           : 2018/9/17 下午1:57
# @Author         : Cirno
# @File           : router.py
# @Description    ：
from handlers.index import IndexHandler
from handlers.user import UserHandlers
from handlers.goods import GoodsHandlers

routers = [(r'/', IndexHandler),
           (r'/user', UserHandlers),
           (r'/goods', GoodsHandlers)]
